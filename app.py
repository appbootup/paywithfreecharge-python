from Crypto.Cipher import AES
import json
import hashlib
import binascii

pad = lambda s: s + (16 - len(s) % 16) * chr(16 - len(s) % 16)
key = 'xxx-xxxx-xxxx-xxxx-xxxxxx'

def checksum(payload):
    payload = {k: v for k, v in payload.items() if (v != None and v != "")}
    jsonSortedWithnoNoneAndEmpty = json.dumps(payload, default=lambda o: o.__dict__, sort_keys=True, separators=(',', ':'))
    finalStringForChecksum = jsonSortedWithnoNoneAndEmpty+key
    b = finalStringForChecksum.encode('utf-8')
    hash_object = hashlib.sha256(b)
    checksum = hash_object.hexdigest()
    return checksum

def loginToken(accessToken):
    keybytes = bytes(key[0:16], encoding='utf-8')
    raw = pad(accessToken)
    cipher = AES.new( keybytes, AES.MODE_ECB )
    ciphertext = cipher.encrypt( raw )
    return binascii.hexlify(bytearray(ciphertext)).decode('utf-8')

if __name__ == '__main__':
    request={
        "firstName": "Aman",
        "lastName": "Gupta",
        "email": "example@domain.com",
        "merchantId": "testmercant",
        "mobileNumber": "9999999999"
    }
    request["checksum"] = checksum(request)
    print(request)

    print(loginToken("qwerty123"))


