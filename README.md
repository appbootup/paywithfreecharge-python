Python configuration
====================
Make sure python and Flask has installed in your system.

If not then use following commands:

mkdir todo-api
cd todo-api
virtualenv flask
flask/bin/pip install flask


Project Configuration
=====================
Edit  and put secret merchant Key in the file constants.py

merchantKey = XXXXX-ABC-5678-ZZZZ-ABCDE

Python  built-in server
===================
Run the following command in terminal to start localhost web server, assuming 

./app.py


HTTP Request
============
Api endpoint :  http://localhost:5000/api/vi/coi/checksum

HTTP method  :  POST (GET not supported)

Request body :  JSON object(JSON representation of request). All JSON values MUST BE Strings.


Example:

URI: http://localhost:5000/api/vi/coi/checksum

In Body Part:

{"currency":"INR","amount":"100","customNote":"custom note","customerName":"prnav","emal":"example@domain.com","furl":
"www.failureurl.com","merchantId":"abc12345678","merchantTxnId":"pranav1234","metadata":"token:metadata","mobile":"9871999999",
"productInfo":"auth","surl":"www.successurl.com"}

HTTP Response
==============
Response Status : 

1) JSON with checksum

2)  Invalid JSON 

3) Application Server Error

Example:

{"amount": "100", "checksum": "83561c2160a0935a2f5b7445866b8f0bad6b2c9955423f4504e07482a40f78ff", "currency": "INR", "customNote": "custom note", "customerName": "prnav", "emal": "example@domain.com", "furl": "www.failureurl.com", "merchantId": "abc12345678", "merchantTxnId": "pranav1234", "metadata": "token:metadata", "mobile": "9871999999", "productInfo": "auth", "surl": "www.successurl.com"}








